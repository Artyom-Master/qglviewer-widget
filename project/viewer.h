#ifndef VIEWER_H
#define VIEWER_H


#include <QGLViewer/qglviewer.h>
#include <QFile>

class Viewer : public QGLViewer {
    Q_OBJECT
private:
    QString *pointsCloud = nullptr;
public:
  Viewer(QWidget *parent = nullptr);
  void setPointsCloud(QString *points) { pointsCloud = points; };
  virtual void draw() override;
  //virtual void init() override;
/*private Q_SLOTS:
  virtual void drawParaboloid();*/
};

#endif // VIEWER_H
