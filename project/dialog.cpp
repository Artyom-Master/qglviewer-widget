#include "dialog.h"
#include "viewer.h"
#include "ui_dialog.h"

Dialog::Dialog(QWidget *parent)
    : QDialog(parent)
    , ui(new Ui::Dialog)
{
    ui->setupUi(this);
    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(drawFigure()));
}

Dialog::~Dialog()
{
    delete ui;
}

void Dialog::drawFigure()
{
    QFile file("pathToThisTxtFile/Paraboloid.txt");
    file.open(QFile::ReadOnly | QFile::Text);
    QString pointss = file.readAll();
    ui->viewer->setPointsCloud(&pointss);
    file.close();
    ui->viewer->repaint();
}

