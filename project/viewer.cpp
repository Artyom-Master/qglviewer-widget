#include "viewer.h"
#include <QFile>

Viewer::Viewer(QWidget *parent) : QGLViewer(parent)
{
   restoreStateFromFile();
}

void Viewer::draw()
{
    if(pointsCloud == nullptr)
    {
        glBegin(GL_LINES);
        glColor3f(1, 1, 1);
        glVertex3f(1, 0, 0);
        glVertex3f(0, 1, 0);
        glVertex3f(0, 0, 1);
        glEnd();
    } else
    {
        quint32 i = 0;
        QString line;
        QStringList points = pointsCloud->split(QRegExp("[\n]"), Qt::SkipEmptyParts);
        glBegin(GL_POINTS);
        Q_FOREACH(line, points)
        {
            i++;
            glColor3f(1, 0, 0);
            glVertex3f(line.section(" ", 0, 0).toFloat(), line.section(" ", 1, 1).toFloat(), line.section(" ", 2, 2).toFloat());
            if(i == 20000) break;
        }
        glEnd();
    }
}

/*void Viewer::drawParaboloid()
{
    QFile file("C:/Users/arthi/OneDrive/Documents/3D_last/Paraboloid.txt");
    file.open(QFile::ReadOnly | QFile::Text);
    QString pointss = file.readAll();
    setPointsCloud(&pointss);
    QString line;
    QStringList points = pointsCloud->split(QRegExp("[\n]"), Qt::SkipEmptyParts);
    glBegin(GL_POINTS);
    Q_FOREACH(line, points)
    {
        glColor3f(1, 0, 0);
        glVertex3f(line.section(" ", 0, 0).toFloat(), line.section(" ", 1, 1).toFloat(), line.section(" ", 2, 2).toFloat());
    }
    glEnd();
}*/
